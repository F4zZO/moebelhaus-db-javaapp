package Database;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class GUI {
	
	private JFrame GUIFrame;
	
	static JFrame controllingFrame;
	
	private String usernum;
	private String itemKind;
	private String maxPrice;
	private String itemGroup;
	private String material;
	private String keyWord;
	
	private int enterNr;
	private int quantity;
	
	public static JPanel panelBackLogin;
	public static JPanel panelBackMenu;
	public static JPanel panelBackItems;
	public static JPanel panelBackEnd;
	
	public static JComboBox<String> cmbItemGroup;
	public static JComboBox<String> cmbMaterial;
	public static JComboBox<String> cmbItemKind;
	
	public static JTextField txtUsrName;
	public static JTextField txtMaxPrice;
	public static JTextField txtKeyword;
	
	public static JLabel lblName;
	public static JLabel lblItemVar;
	
	static JPasswordField pwPassword;
	
	private static String[] titels = new String[]{"Row", "Name", "Preis", "Artikelnummer"};
	
	public static JCheckBox chckbxRememberMe;

	public static void GUIStart() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.GUIFrame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public GUI() {
		initialize();
	}

	private void initialize() {
		GUIFrame = new JFrame("Möbelhaus");
		GUIFrame.setResizable(false);
		GUIFrame.setVisible(true);
		GUIFrame.setIconImage(Toolkit.getDefaultToolkit().getImage("src/Images/WoodHouse.png"));
		GUIFrame.setBounds(480, 250, 800, 800);
		GUIFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GUIFrame.getContentPane().setLayout(null);
		
		//PANELS-------------------------------------------------------------------------------------------------
		
		panelBackLogin = new JPanel();
		panelBackLogin.setBackground(new Color(32, 32, 32));
		panelBackLogin.setBounds(0, 0, 800, 800);
		GUIFrame.getContentPane().add(panelBackLogin);
		panelBackLogin.setLayout(null);
		
		panelBackMenu = new JPanel();
		panelBackMenu.setBackground(new Color(32, 32, 32));
		panelBackMenu.setBounds(0, 0, 800, 800);
		GUIFrame.getContentPane().add(panelBackMenu);
		panelBackMenu.setLayout(null);
		panelBackMenu.setVisible(false);
		
		panelBackItems = new JPanel();
		panelBackItems.setBackground(new Color(32, 32, 32));
		panelBackItems.setBounds(0, 0, 800, 800);
		GUIFrame.getContentPane().add(panelBackItems);
		panelBackItems.setLayout(null);
		panelBackItems.setVisible(false);
		
		panelBackEnd = new JPanel();
		panelBackEnd.setBackground(new Color(32, 32, 32));
		panelBackEnd.setBounds(0, 0, 800, 800);
		GUIFrame.getContentPane().add(panelBackEnd);
		panelBackEnd.setLayout(null);
		panelBackEnd.setVisible(false);
		
		//ITEMS LOGIN---------------------------------------------------------------------------------------------
		
		JLabel lblSignIn = new JLabel("MÖBELHAUS");
		lblSignIn.setHorizontalAlignment(SwingConstants.CENTER);
		lblSignIn.setFont(new Font("Bookman Old Style", Font.PLAIN, 28));
		lblSignIn.setForeground(new Color(255, 255, 153));
		lblSignIn.setBounds(295, 150, 210, 35);
		panelBackLogin.add(lblSignIn);
		
		JLabel lblUsrName = new JLabel("Kundennummer:");
		lblUsrName.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblUsrName.setForeground(new Color(255, 255, 153));
		lblUsrName.setBounds(295, 220, 210, 35);
		panelBackLogin.add(lblUsrName);
		
		JLabel lblUsrNameErr = new JLabel("");
		lblUsrNameErr.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblUsrNameErr.setForeground(new Color(255, 0, 0));
		lblUsrNameErr.setBounds(400, 220, 250, 35);
		panelBackLogin.add(lblUsrNameErr);
		
		txtUsrName = new JTextField();
		txtUsrName.setForeground(Color.WHITE);
		txtUsrName.setBackground(new Color(10, 10, 10));
		txtUsrName.setBounds(295, 250, 210, 35);
		panelBackLogin.add(txtUsrName);
		txtUsrName.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent keyEvtLogin) {
				char c = keyEvtLogin.getKeyChar();
				if (c >= '0' && c <= '9' || c == KeyEvent.VK_BACK_SPACE) {
	            	txtUsrName.setEditable(true);
	            	lblUsrNameErr.setText("");
	            } else {
	            	txtUsrName.setEditable(false);
	            	lblUsrNameErr.setText("* Bitte nur Dezimalzahlen eingeben! (0-9)");
	            }
	         }
	      });
		
		JButton btnSignIn = new JButton("Einloggen");
		btnSignIn.setBackground(Color.DARK_GRAY);
		btnSignIn.setForeground(Color.WHITE);
		btnSignIn.setFont(new Font("SansSerif", Font.PLAIN, 18));
		btnSignIn.setBounds(295, 550, 210, 35);
		btnSignIn.setEnabled(true);
		panelBackLogin.add(btnSignIn);
		
		btnSignIn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				usernum = txtUsrName.getText();
				if(checkEntry(usernum)) {
					Logic.login(Integer.parseInt(usernum));
				}
			}	
		});
		
		//ITEMS Menu---------------------------------------------------------------------------------------------
		
		JLabel lblOrder = new JLabel("Bestellung");
		lblOrder.setHorizontalAlignment(SwingConstants.CENTER);
		lblOrder.setFont(new Font("Bookman Old Style", Font.PLAIN, 28));
		lblOrder.setForeground(new Color(255, 255, 153));
		lblOrder.setBounds(295, 40, 210, 35);
		panelBackMenu.add(lblOrder);
		
		lblName = new JLabel("TST");
		lblName.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblName.setForeground(new Color(255, 255, 153));
		lblName.setBounds(60, 120, 210, 35);
		panelBackMenu.add(lblName);
		
		JLabel lblItemKind = new JLabel("Artikelart angeben:");
		lblItemKind.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblItemKind.setForeground(new Color(255, 255, 153));
		lblItemKind.setBounds(60, 200, 210, 35);
		panelBackMenu.add(lblItemKind);
		
		cmbItemKind = new JComboBox<String>();
		cmbItemKind.setBounds(60, 230, 210, 35);
		cmbItemKind.setForeground(Color.WHITE);
		cmbItemKind.setBackground(new Color(10, 10, 10));
		panelBackMenu.add(cmbItemKind);
		cmbItemKind.addItem("Hausratsartikel");
		cmbItemKind.addItem("Möbelstück");
		
		JLabel lblMaxPrice = new JLabel("Maximaler Preis angeben:");
		lblMaxPrice.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblMaxPrice.setForeground(new Color(255, 255, 153));
		lblMaxPrice.setBounds(60, 300, 210, 35);
		panelBackMenu.add(lblMaxPrice);
		
		JLabel lblMaxPriceErr = new JLabel("");
		lblMaxPriceErr.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblMaxPriceErr.setForeground(new Color(255, 0, 0));
		lblMaxPriceErr.setBounds(200, 300, 250, 35);
		panelBackMenu.add(lblMaxPriceErr);
		
		txtMaxPrice = new JTextField();
		txtMaxPrice.setForeground(Color.WHITE);
		txtMaxPrice.setBackground(new Color(10, 10, 10));
		txtMaxPrice.setBounds(60, 330, 210, 35);
		panelBackMenu.add(txtMaxPrice);
		txtMaxPrice.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent keyEvtPrice) {
	        	 char c = keyEvtPrice.getKeyChar();
		         if (c >= '0' && c <= '9' || c == KeyEvent.VK_BACK_SPACE) {
	            	txtMaxPrice.setEditable(true);
	            	lblMaxPriceErr.setText("");
	            } else {
	            	txtMaxPrice.setEditable(false);
	            	lblMaxPriceErr.setText("* Bitte nur Dezimalzahlen eingeben! (0-9)");
	            }
	         }
	      });
		
		JLabel lblAddition = new JLabel("Artikelgruppe");
		lblAddition.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblAddition.setForeground(new Color(255, 255, 153));
		lblAddition.setBounds(60, 400, 210, 35);
		panelBackMenu.add(lblAddition);
		
		cmbItemGroup = new JComboBox<String>();
		cmbItemGroup.setBounds(60, 430, 210, 35);
		cmbItemGroup.setForeground(Color.WHITE);
		cmbItemGroup.setBackground(new Color(10, 10, 10));
		cmbItemGroup.setVisible(true);
		panelBackMenu.add(cmbItemGroup);
		
		cmbMaterial = new JComboBox<String>();
		cmbMaterial.setBounds(60, 430, 210, 35);
		cmbMaterial.setForeground(Color.WHITE);
		cmbMaterial.setBackground(new Color(10, 10, 10));
		cmbMaterial.setVisible(false);
		panelBackMenu.add(cmbMaterial);
		
		cmbItemKind.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				if(cmbItemKind.getSelectedIndex()==0) {
					cmbItemGroup.setVisible(true);
					cmbMaterial.setVisible(false);
					lblAddition.setText("Artikelgruppe:");
				}
				if(cmbItemKind.getSelectedIndex()==1) {
					cmbItemGroup.setVisible(false);
					cmbMaterial.setVisible(true);
					lblAddition.setText("Holzart:");
				}
			}
		});
		
		JLabel lblKeyword= new JLabel("Stichwort:");
		lblKeyword.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblKeyword.setForeground(new Color(255, 255, 153));
		lblKeyword.setBounds(60, 500, 210, 35);
		lblKeyword.setVisible(false);
		panelBackMenu.add(lblKeyword);
		
		txtKeyword = new JTextField();
		txtKeyword.setForeground(Color.WHITE);
		txtKeyword.setBackground(new Color(10, 10, 10));
		txtKeyword.setBounds(60, 530, 210, 35);
		txtKeyword.setVisible(false);
		panelBackMenu.add(txtKeyword);
		
		JButton btnSearch = new JButton("Suche");
		btnSearch.setBackground(Color.DARK_GRAY);
		btnSearch.setForeground(Color.WHITE);
		btnSearch.setFont(new Font("SansSerif", Font.PLAIN, 18));
		btnSearch.setBounds(295, 700, 210, 35);
		btnSearch.setEnabled(true);
		panelBackMenu.add(btnSearch);
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				itemKind = (String) cmbItemKind.getSelectedItem();
				maxPrice = txtMaxPrice.getText();
				itemGroup = (String) cmbItemGroup.getSelectedItem();
				material = (String) cmbMaterial.getSelectedItem();
				keyWord = txtKeyword.getText();
				
				if(maxPrice.equals("")) {
					maxPrice = "10000";
				}
				
				if(checkEntry(maxPrice)) {
					Logic.search(itemKind, maxPrice, itemGroup, material, keyWord);
					WindowHandler.openItems();
				}
			}	
		});
		
		JButton btnBreak = new JButton("Abbrechen");
		btnBreak.setBackground(Color.DARK_GRAY);
		btnBreak.setForeground(Color.WHITE);
		btnBreak.setFont(new Font("SansSerif", Font.PLAIN, 18));
		btnBreak.setBounds(60, 700, 210, 35);
		btnBreak.setEnabled(true);
		panelBackMenu.add(btnBreak);
		btnBreak.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				WindowHandler.cancel("menu");
			}	
		});
		
		JButton btnEnd = new JButton("Bestätigen");
		btnEnd.setBackground(Color.DARK_GRAY);
		btnEnd.setForeground(Color.WHITE);
		btnEnd.setFont(new Font("SansSerif", Font.PLAIN, 18));
		btnEnd.setBounds(530, 700, 210, 35);
		btnEnd.setEnabled(true);
		panelBackMenu.add(btnEnd);
		btnEnd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				WindowHandler.confirm();
			}	
		});
		
		cmbItemKind.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if(cmbItemKind.getSelectedIndex()==0) {
					lblKeyword.setVisible(false);
					txtKeyword.setVisible(false);
				}else {
					lblKeyword.setVisible(true);
					txtKeyword.setVisible(true);
				}
			}
		});
		
		//ITEMS Items---------------------------------------------------------------------------------------------
		
		JLabel lblTabel = new JLabel("Artikel");
		lblTabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblTabel.setFont(new Font("Bookman Old Style", Font.PLAIN, 28));
		lblTabel.setForeground(new Color(255, 255, 153));
		lblTabel.setBounds(295, 40, 210, 35);
		panelBackItems.add(lblTabel);
		
		JLabel lblTblNr = new JLabel("Nummer");
		lblTblNr.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblTblNr.setForeground(new Color(255, 255, 153));
		lblTblNr.setBounds(60, 90, 210, 35);
		panelBackItems.add(lblTblNr);
		
		JLabel lblItemName = new JLabel("Artikelname");
		lblItemName.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblItemName.setForeground(new Color(255, 255, 153));
		lblItemName.setBounds(230, 90, 210, 35);
		panelBackItems.add(lblItemName);
		
		JLabel lblPrice = new JLabel("Preis");
		lblPrice.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblPrice.setForeground(new Color(255, 255, 153));
		lblPrice.setBounds(400, 90, 210, 35);
		panelBackItems.add(lblPrice);
		
		lblItemVar = new JLabel("");
		lblItemVar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblItemVar.setForeground(new Color(255, 255, 153));
		lblItemVar.setBounds(570, 90, 210, 35);
		panelBackItems.add(lblItemVar);
		
		JTable tblItems = new JTable(Logic.items, titels);
		tblItems.setBackground(new Color(32, 32, 32));
		tblItems.setForeground(Color.WHITE);
		tblItems.setEnabled(false);
		tblItems.setShowVerticalLines(false);
		tblItems.setShowHorizontalLines(false);
		tblItems.setShowGrid(false);
		tblItems.setBounds(60, 120, 680, 300);
		panelBackItems.add(tblItems);
		
		JLabel lblEnterNr = new JLabel("Nummer des Artikels eingeben:");
		lblEnterNr.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblEnterNr.setForeground(new Color(255, 255, 153));
		lblEnterNr.setBounds(295, 450, 210, 35);
		panelBackItems.add(lblEnterNr);
		
		JLabel lblEnterNrErr = new JLabel();
		lblEnterNrErr.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblEnterNrErr.setForeground(new Color(255, 0, 0));
		lblEnterNrErr.setBounds(350, 450, 210, 35);
		panelBackItems.add(lblEnterNrErr);
		
		JTextField txtEnterNr = new JTextField();
		txtEnterNr.setForeground(Color.WHITE);
		txtEnterNr.setBackground(new Color(10, 10, 10));
		txtEnterNr.setBounds(295, 480, 210, 35);
		panelBackItems.add(txtEnterNr);
		txtEnterNr.addKeyListener(new KeyAdapter() {
	         public void keyPressed(KeyEvent keyEvtEnterNr) {
	        	 char c = keyEvtEnterNr.getKeyChar();
		         if (c >= '0' && c <= '9' || c == KeyEvent.VK_BACK_SPACE) {
	            	txtEnterNr.setEditable(true);
	            	lblEnterNrErr.setText("");
	            } else {
	            	txtEnterNr.setEditable(false);
	            	lblEnterNrErr.setText("* Bitte nur Dezimalzahlen eingeben! (0-9)");
	            }
	         }
	      });
		
		JLabel lblEnterAmt = new JLabel("Menge:");
		lblEnterAmt.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblEnterAmt.setForeground(new Color(255, 255, 153));
		lblEnterAmt.setBounds(295, 520, 210, 35);
		panelBackItems.add(lblEnterAmt);
		
		JLabel lblEnterAmtErr = new JLabel();
		lblEnterAmtErr.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblEnterAmtErr.setForeground(new Color(255, 0, 0));
		lblEnterAmtErr.setBounds(350, 520, 210, 35);
		panelBackItems.add(lblEnterAmtErr);
		
		JTextField txtEnterAmt = new JTextField();
		txtEnterAmt.setForeground(Color.WHITE);
		txtEnterAmt.setBackground(new Color(10, 10, 10));
		txtEnterAmt.setBounds(295, 550, 210, 35);
		panelBackItems.add(txtEnterAmt);
		txtEnterAmt.addKeyListener(new KeyAdapter() {
	         public void keyPressed(KeyEvent keyEvtEnterAmt) {
	        	char c = keyEvtEnterAmt.getKeyChar();
	            if (c >= '0' && c <= '9' || c == KeyEvent.VK_BACK_SPACE) {
	            	txtEnterAmt.setEditable(true);
	            	lblEnterAmtErr.setText("");
	            } else {
	            	txtEnterAmt.setEditable(false);
	            	lblEnterAmtErr.setText("* Bitte nur Dezimalzahlen eingeben! (0-9)");
	            }
	         }
	      });
		JButton btnEnterNr = new JButton("Abbrechen");
		btnEnterNr.setBackground(Color.DARK_GRAY);
		btnEnterNr.setForeground(Color.WHITE);
		btnEnterNr.setFont(new Font("SansSerif", Font.PLAIN, 18));
		btnEnterNr.setBounds(150, 700, 210, 35);
		btnEnterNr.setEnabled(true);
		panelBackItems.add(btnEnterNr);
		btnEnterNr.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				WindowHandler.cancel("items");
			}	
		});
		JButton btnItemsCancel = new JButton("Hinzufügen");
		btnItemsCancel.setBackground(Color.DARK_GRAY);
		btnItemsCancel.setForeground(Color.WHITE);
		btnItemsCancel.setFont(new Font("SansSerif", Font.PLAIN, 18));
		btnItemsCancel.setBounds(420, 700, 210, 35);
		btnItemsCancel.setEnabled(true);
		panelBackItems.add(btnItemsCancel);
		btnItemsCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				enterNr = Integer.parseInt(txtEnterNr.getText());
				
				if(txtEnterAmt.getText().equals("")) {
					txtEnterAmt.setText("1");
				}
				quantity = Integer.parseInt(txtEnterAmt.getText());
				
				if(Logic.itemAnz < enterNr) {
					WindowHandler.fail("wrong");
				}else if(checkEntry(txtEnterAmt.getText()) && checkEntry(txtEnterNr.getText())) {
					Logic.add(enterNr, quantity);
					WindowHandler.openMenu();
					txtEnterAmt.setText("");
					txtEnterNr.setText("");
				}
				
			}	
		});
		
		//ITEMS End---------------------------------------------------------------------------------------------
		
		JLabel lblEnd = new JLabel("Danke für ihre Bestellung!");
		lblEnd.setHorizontalAlignment(SwingConstants.CENTER);
		lblEnd.setFont(new Font("Bookman Old Style", Font.PLAIN, 28));
		lblEnd.setForeground(new Color(255, 255, 153));
		lblEnd.setBounds(200, 100, 400, 35);
		panelBackEnd.add(lblEnd);
		
		JButton btnEndMenu = new JButton("Zurück zum Menü");
		btnEndMenu.setBackground(Color.DARK_GRAY);
		btnEndMenu.setForeground(Color.WHITE);
		btnEndMenu.setFont(new Font("SansSerif", Font.PLAIN, 18));
		btnEndMenu.setBounds(295, 500, 210, 35);
		btnEndMenu.setEnabled(true);
		panelBackEnd.add(btnEndMenu);
		btnEndMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				rst();
			}	
		});
		
		//INIT----------------------------------------------------------------------------------------------------
		
		WindowHandler.openLogin();
	}
	public static void rst() {				//reset all texts and selected boxes and reinitializes
		Logic.init();
		txtUsrName.setText("");
		txtMaxPrice.setText("");
		txtKeyword.setText("");
		cmbItemKind.setSelectedIndex(0);
	}
	
	private boolean checkEntry(String entry) {
		if(entry.equals("")) {
			WindowHandler.fail("wrong");
			return false;
		}
		try {
			Integer.parseInt(entry);
		}catch(NumberFormatException ex) {
			WindowHandler.fail("wrong");
			return false;
		}
		return true;
	}
}