package Database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Logic {
	private static Connection conn = SQLConnector.connection();
	static java.sql.PreparedStatement preparedStatement = null;
	
	static String name;
	static String itemKind;
	public static int itemAnz;
	
	private static String date;
	private static boolean loginFlag;
	private static boolean searchFlag;
	private static boolean addFlag;
	
	private static int[][] arrHausrat = new int[10][3];
	private static int[][] arrMoebel = new int[10][3];
	
	private static int anzHausrat = 0;
	private static int anzMoebel = 0;
	
	private static int itemID;
	private static int ID_A_B = 1;
	private static int ID_Bestellung = 1;
	private static int ID_Kunde = 1;
	private static double rechnung = 0;
	
	public static String[][] items = new String[5][10];
	
	static void init(){		//Initializes comboboxes 
		
		GUI.cmbItemGroup.removeAllItems();
		GUI.cmbMaterial.removeAllItems();
		addFlag = false;
		String queryInit = "SELECT DISTINCT Artikelgruppe FROM hausratsartikel ORDER BY Artikelgruppe";
		try(Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(queryInit)){
				while (rs.next()) {
					GUI.cmbItemGroup.addItem(rs.getString("Artikelgruppe"));	
				}
		} catch (SQLException e) {
			e.printStackTrace();
			WindowHandler.fail("SQLinit");
			return;
		}
		
		queryInit = "SELECT DISTINCT Material FROM moebelstuecke ORDER BY Material";
		try(Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(queryInit)){
			while (rs.next()) {
				GUI.cmbMaterial.addItem(rs.getString("Material"));	
			}
		} catch (SQLException e) {
			e.printStackTrace();
			WindowHandler.fail("SQLinit1");
			return;
		}
		
		String queryAddId = "SELECT * FROM tab_a_b";
		try(Statement stmtOr = conn.createStatement();
			ResultSet rsOr = stmtOr.executeQuery(queryAddId)){
			
			int max = 0;
			while(rsOr.next()) {
				if(max<rsOr.getInt("ID_A_B")) {
					max = rsOr.getInt("ID_A_B");
				}
			}
			ID_A_B = max+1;
			WindowHandler.openMenu();			
		} catch (SQLException e) {
			e.printStackTrace();
			WindowHandler.fail("SQLadd");
			return;
		}
		
		anzHausrat = 0;
		anzMoebel = 0;
		
		for(int i=0; i<arrHausrat.length; i++) {
			arrHausrat[i][0] = 0;
			arrHausrat[i][1] = 0;
			arrHausrat[i][2] = 0;
		}
		for(int i=0; i<arrMoebel.length; i++) {
			arrMoebel[i][0] = 0;
			arrMoebel[i][1] = 0;
			arrMoebel[i][2] = 0;
		}
	}
	
	static void login(int usernum){		//checks for User and writes down IDs	
		loginFlag = false;
		ID_Kunde = usernum;
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
		LocalDateTime now = LocalDateTime.now();  
		date = dtf.format(now);
		
		String queryLogin = "SELECT * FROM kunde WHERE ID_Kunde = ?";
		try(PreparedStatement ps = conn.prepareStatement(queryLogin)){
				ps.setInt(1, usernum);
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					name = rs.getString("Nachname");
					loginFlag = true;
				}
		} catch (SQLException e) {
			e.printStackTrace();
			WindowHandler.fail("SQLuser");
			return;
		}
		if(!loginFlag) {
			WindowHandler.fail("login");
			return;
		}

		GUI.lblName.setText(name);
		
		String queryOrder = "SELECT * FROM bestellung";
		try(Statement stmtOr = conn.createStatement();
			ResultSet rsOr = stmtOr.executeQuery(queryOrder)){
			
			int max = 0;
			while(rsOr.next()) {
				if(max<rsOr.getInt("ID_Bestellung")) {
					max = rsOr.getInt("ID_Bestellung");
				}
			}
			ID_Bestellung = max+1;
			WindowHandler.openMenu();			
		} catch (SQLException e) {
			e.printStackTrace();
			WindowHandler.fail("SQLuser2");
			return;
		}
	}
	
	static void search(String itemKind, String maxPrice, String itemGroup, String material, String keyWord){	//divides into hausratsartikel or m�bel and selects all items and puts them into array
		Logic.itemKind = itemKind;
		String querySearch = "";
		String tempB = "";
		searchFlag = false;
		
		if(itemKind.equals("Hausratsartikel")) {
			tempB = itemGroup;
			if(keyWord.equals("")) {
				querySearch = "SELECT * FROM Hausratsartikel WHERE Preis < ? AND Artikelgruppe = ?";
			} else {
				querySearch = "SELECT * FROM Hausratsartikel WHERE Preis < ? AND Artikelgruppe = ? AND Beschreibung LIKE ?";
			}
		}else if(itemKind.equals("M�belst�ck")) {
			tempB = material;
			if(keyWord.equals("")) {
				querySearch = "SELECT * FROM Moebelstuecke WHERE Preis < ? AND Material = ?";
			} else {
				querySearch = "SELECT * FROM Moebelstuecke WHERE Preis < ? AND Material = ? AND Beschreibung LIKE ?";
			}
		}
	
		try(PreparedStatement ps = conn.prepareStatement(querySearch)){
			ps.setString(1, maxPrice);
			ps.setString(2, tempB);
			if(!keyWord.equals("")) {
				ps.setString(3, keyWord);
			}
			ResultSet rs = ps.executeQuery();
			
			if(itemKind.equals("Hausratsartikel")) {
				int i = 0;	
				while (rs.next()) {
					String name = rs.getString("Name");
					String price = rs.getString("Preis");
					String itemGrp = rs.getString("Artikelgruppe");
					int id = rs.getInt("ID_Artikel");
					rechnung = rechnung + rs.getDouble("Preis");
					
					items[i][0] = Integer.toString(i+1);
					items[i][1] = name;
					items[i][2] = price;
					items[i][3] = itemGrp;
					items[i][4] = Integer.toString(id);
					i++;
					searchFlag = true;
					GUI.lblItemVar.setText("Artikelgruppe:");
					itemAnz = i;
				}
			}else if(itemKind.equals("M�belst�ck")) {
				int i = 0;	
				while (rs.next()) {
					String name = rs.getString("Name");
					String price = rs.getString("Preis");
					String mat = rs.getString("Material");
					int id = rs.getInt("ID_Artikel");
					rechnung = rechnung + rs.getDouble("Preis");
					
					items[i][0] = Integer.toString(i+1);
					items[i][1] = name;
					items[i][2] = price;
					items[i][3] = mat;
					items[i][4] = Integer.toString(id);
					i++;
					searchFlag = true;
					GUI.lblItemVar.setText("Material:");
					itemAnz = i;
				}
			}
			if(!searchFlag) {
				WindowHandler.fail("search");
			}
		} catch (SQLException e) {
			e.printStackTrace();
			WindowHandler.fail("SQLsearch");
			return;
		}
	}
	
	static void add(int itemsIndex, int quantity){		//gets selected id and ads it into connecting table
		
		itemID = Integer.parseInt(items[itemsIndex-1][4]);
		
		if(Logic.itemKind.equals("Hausratsartikel")) {
			arrHausrat[anzHausrat][0] = ID_A_B;
			arrHausrat[anzHausrat][1] = itemID;
			arrHausrat[anzHausrat][2] = quantity;
			anzHausrat++;
			ID_A_B++;
			
		}else if(Logic.itemKind.equals("M�belst�ck")) {
			arrMoebel[anzMoebel][0] = ID_A_B;
			arrMoebel[anzMoebel][1] = itemID;
			arrMoebel[anzMoebel][2] = quantity;
			anzMoebel++;
			ID_A_B++;
		}else {
			WindowHandler.fail("add");
			return;
		}
		addFlag = true;
	}
	
	static void end(){	//adds Bestellung
		
		if(!addFlag) {
			WindowHandler.fail("empty");
			return;
		}
		
		String queryend = "INSERT INTO BESTELLUNG(ID_Bestellung, Datum, Rechnung, fk_kunde) VALUES (?, ?, ?, ?)";
		try (PreparedStatement ps = conn.prepareStatement(queryend);){
			ps.setInt(1, ID_Bestellung);
			ps.setString(2, date);
			ps.setDouble(3, rechnung);
			ps.setInt(4, ID_Kunde);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			WindowHandler.fail("SQLend");
			return;
		}
		
		queryend = "INSERT INTO tab_a_b(ID_A_B, fk_bestellung, fk_hausrat, Anzahl) VALUES (?, ?, ?, ?)";
		int i = 0;
		while(arrHausrat[i][0] != 0) {
			try (PreparedStatement ps = conn.prepareStatement(queryend)){
				ps.setInt(1, arrHausrat[i][0]);
				ps.setInt(2, ID_Bestellung);
				ps.setInt(3, arrHausrat[i][1]);
				ps.setInt(4, arrHausrat[i][2]);
				ps.executeUpdate();
			} catch(SQLException e) {
				e.printStackTrace();
				WindowHandler.fail("SQLadd");
				return;
			}
			i++;
		}
		
		queryend = "INSERT INTO tab_a_b(ID_A_B, fk_bestellung, fk_moebel, Anzahl) VALUES (?, ?, ?, ?)";
		i = 0;
		while(arrMoebel[i][0] != 0) {
			try (PreparedStatement ps = conn.prepareStatement(queryend)){
				ps.setInt(1, arrMoebel[i][0]);
				ps.setInt(2, ID_Bestellung);
				ps.setInt(3, arrMoebel[i][1]);
				ps.setInt(4, arrMoebel[i][2]);
				ps.executeUpdate();
			} catch(SQLException e) {
				e.printStackTrace();
				WindowHandler.fail("SQLadd");
				return;
			}
			i++;
		}
	}
}
