package Database;

//Frederic W�rz - Datenbanksysteme WS2020

import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class WindowHandler {

	public static void main(String[] args) {
		GUI.GUIStart();
	}
	
	static void openLogin() {
		GUI.rst();
		GUI.panelBackLogin.setVisible(true);
		GUI.panelBackMenu.setVisible(false);
		GUI.panelBackItems.setVisible(false);
		GUI.panelBackEnd.setVisible(false);
	}
	
	static void openMenu() {
		GUI.panelBackLogin.setVisible(false);
		GUI.panelBackMenu.setVisible(true);
		GUI.panelBackItems.setVisible(false);
		GUI.panelBackEnd.setVisible(false);
	}
	
	static void openItems() {
		GUI.panelBackLogin.setVisible(false);
		GUI.panelBackMenu.setVisible(false);
		GUI.panelBackItems.setVisible(true);
		GUI.panelBackEnd.setVisible(false);
	}
	
	static void openEnd() {
		GUI.panelBackLogin.setVisible(false);
		GUI.panelBackMenu.setVisible(false);
		GUI.panelBackItems.setVisible(false);
		GUI.panelBackEnd.setVisible(true);
	}
	
	static void confirm() {		//Confirmationwindow
		JFrame logout = new JFrame("BEST�TIGEN");
		logout.setIconImage(Toolkit.getDefaultToolkit().getImage("src/Images/WoodHouse.png"));
		logout.setVisible(true);
		logout.setResizable(false);
		logout.setBounds(700, 600, 400, 200);
		
		JPanel panelSureBackgr1 = new JPanel();
		panelSureBackgr1.setBounds(0, 0, 400, 200);
		panelSureBackgr1.setBackground(new Color(32, 32, 32));
		logout.getContentPane().add(panelSureBackgr1);
		panelSureBackgr1.setLayout(null);
		
		JLabel lblSure = new JLabel("Sind Sie fertig?");
		lblSure.setBackground(new Color(5, 10, 35));
		lblSure.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblSure.setHorizontalAlignment(SwingConstants.CENTER);
		lblSure.setForeground(new Color(255, 255, 153));
		lblSure.setBounds(90, 20, 220, 20);
		panelSureBackgr1.add(lblSure);
		
		JButton btnYes = new JButton("Ja");
		btnYes.setBackground(Color.DARK_GRAY);
		btnYes.setForeground(Color.WHITE);
		btnYes.setBounds(50, 100, 100, 30);
		panelSureBackgr1.add(btnYes);
		btnYes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logout.dispose();
				Logic.end();
				openLogin();
			}
		});
		
		JButton btnNo = new JButton("NEIN");
		btnNo.setBackground(Color.DARK_GRAY);
		btnNo.setForeground(Color.WHITE);
		btnNo.setBounds(250, 100, 100, 30);
		panelSureBackgr1.add(btnNo);
		btnNo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logout.dispose();
			}
		});
	}

	static void cancel(String state) {		//Cancelwindow
		JFrame logout = new JFrame("ABBRECHEN");
		logout.setIconImage(Toolkit.getDefaultToolkit().getImage("src/Images/WoodHouse.png"));
		logout.setVisible(true);
		logout.setResizable(false);
		logout.setBounds(700, 600, 400, 200);
		
		JPanel panelSureBackgr1 = new JPanel();
		panelSureBackgr1.setBounds(0, 0, 400, 200);
		panelSureBackgr1.setBackground(new Color(32, 32, 32));
		logout.getContentPane().add(panelSureBackgr1);
		panelSureBackgr1.setLayout(null);
		
		JLabel lblSure = new JLabel();
		lblSure.setBackground(new Color(5, 10, 35));
		lblSure.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblSure.setHorizontalAlignment(SwingConstants.CENTER);
		lblSure.setForeground(new Color(255, 255, 153));
		lblSure.setBounds(90, 20, 220, 20);
		panelSureBackgr1.add(lblSure);
		
		if(state.equals("menu")) {
			lblSure.setText("Wollen Sie die Bestellung abbrechen?");
		}else if(state.equals("items")) {
			lblSure.setText("Wollen Sie die Suche abbrechen?");
		}
		
		JButton btnYes = new JButton("Ja");
		btnYes.setBackground(Color.DARK_GRAY);
		btnYes.setForeground(Color.WHITE);
		btnYes.setBounds(50, 100, 100, 30);
		panelSureBackgr1.add(btnYes);
		btnYes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logout.dispose();
				if(state.equals("menu")) {
					openLogin();
				}else if(state.equals("items")) {
					openMenu();
				}
				
			}
		});
		
		JButton btnNo = new JButton("NEIN");
		btnNo.setBackground(Color.DARK_GRAY);
		btnNo.setForeground(Color.WHITE);
		btnNo.setBounds(250, 100, 100, 30);
		panelSureBackgr1.add(btnNo);
		btnNo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logout.dispose();
			}
		});
	}
	
	static void fail(String reason)	{		//handles all my error windows
		JFrame logout = new JFrame("Failed");
		logout.setIconImage(Toolkit.getDefaultToolkit().getImage("src/Images/WoodHouse.png"));
		logout.setVisible(true);
		logout.setResizable(false);
		logout.setBounds(700, 600, 400, 200);
		
		JPanel panelSureBackgr1 = new JPanel();
		panelSureBackgr1.setBounds(0, 0, 400, 200);
		panelSureBackgr1.setBackground(new Color(32, 32, 32));
		logout.getContentPane().add(panelSureBackgr1);
		panelSureBackgr1.setLayout(null);
		
		JLabel lblSure = new JLabel("Failed!");
		lblSure.setBackground(new Color(5, 10, 35));
		lblSure.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblSure.setHorizontalAlignment(SwingConstants.CENTER);
		lblSure.setForeground(new Color(255, 255, 153));
		lblSure.setBounds(75, 20, 250, 20);
		panelSureBackgr1.add(lblSure);
		
		if(reason.equals("login")) {
			logout.setTitle("LOGIN FEHLGESCHLAGEN");
			lblSure.setText("Kundennummer nicht gefunden!");
		}else if(reason.equals("wrong")) {
			logout.setTitle("FALSCHE EINGABE");
			lblSure.setText("Bitte nur erlaubte Zeichen eingeben!");			
		}
		else if(reason.equals("search")) {
			logout.setTitle("KEIN PRODUKT GEFUNDEN");
			lblSure.setText("Das Produkt wurde nicht gefunden!");			
		}
		else if(reason.equals("add")) {
			logout.setTitle("UNEXPECTED ERROR ADD");
			lblSure.setText("Unexpected error in Add!");			
		}
		else if(reason.equals("empty")) {
			logout.setTitle("WARENKORB IST LEER");
			lblSure.setText("Keine Produkte ausgew�hlt!");			
		}
		else if(reason.equals("SQLinit")) {
			logout.setTitle("INITIALIZING");
			lblSure.setText("Initializing SQL error!");			
		}
		else if(reason.equals("SQLinit2")) {
			logout.setTitle("INITIALIZING MATERIAL");
			lblSure.setText("Initializing Material SQL error!");			
		}
		else if(reason.equals("SQLuser")) {
			logout.setTitle("USER");
			lblSure.setText("User SQL error!");			
		}
		else if(reason.equals("SQLuser2")) {
			logout.setTitle("USER BESTELLUNG");
			lblSure.setText("User Bestellung SQL error!");			
		}
		else if(reason.equals("SQLsearch")) {
			logout.setTitle("SEARCH");
			lblSure.setText("Seach SQL error!");			
		}
		else if(reason.equals("SQLadd")) {
			logout.setTitle("ADD");
			lblSure.setText("Add SQL error ID!");			
		}
		else if(reason.equals("SQLadd2")) {
			logout.setTitle("ADD");
			lblSure.setText("Add SQL error!");			
		}
		else if(reason.equals("SQLend")) {
			logout.setTitle("END");
			lblSure.setText("End SQL error!");			
		}
		
		JButton btnOk = new JButton("OK");
		btnOk.setBackground(Color.DARK_GRAY);
		btnOk.setForeground(Color.WHITE);
		btnOk.setBounds(150, 100, 100, 30);
		panelSureBackgr1.add(btnOk);
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logout.dispose();
			}
		});
	}
}