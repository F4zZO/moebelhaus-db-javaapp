package Database;

import java.sql.*;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class SQLConnector{		//SQL Connection

	static String driver = "com.mysql.jbdc.Driver";
	static String url = "jdbc:oracle:thin:@localhost:10111:namib";
	static String user = "DABS_40";
	static String password = "DABS_40";
	
	static Connection conn;
	private static JFrame controllingFrame;
	
	public static Connection connection( ) {
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(url, user, password);
		} catch (SQLException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(controllingFrame,
					"Verbindung zur Datenbank Fehlgeschlagen.", "Fehler!",
					JOptionPane.ERROR_MESSAGE);
		}
		return conn;
	}
}